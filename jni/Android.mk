LOCAL_PATH:= $(call my-dir)

APP_ABI := arm-v7a

include $(CLEAR_VARS)


LOCAL_MODULE    := wrapsigs
LOCAL_CFLAGS    := -Werror
LOCAL_SRC_FILES := wrapsigs.cpp
LOCAL_LDLIBS    := -llog 

include $(BUILD_SHARED_LIBRARY)